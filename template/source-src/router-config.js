export const routes = [{
    path: '/',
    component: require('./view/index.vue')
}, {
    path: '*',
    redirect: '/'
}]
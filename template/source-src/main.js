// Dependências do NodeJS
import Vue from 'vue'
import VueRouter from 'vue-router'

import jQuery from 'jquery/dist/jquery.slim'

// Expondo Global
window.Vue = Vue
window.$ = jQuery

// Dependências Externas
import App from './App.vue'
import {routes} from './router-config'

// Instalação dos Plugins
Vue.use(VueRouter)

// Atribuir o novo roteador
const router = new VueRouter({
    routes,
    mode: 'history',
    saveScrollPosition: true,
    linkActiveClass: 'active'
})

// Iniciar o aplicativo
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')